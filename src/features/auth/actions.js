import * as ActionType from "./actionTypes";
import { api } from "../../common";

export const login = (email, password) => {
  return dispatch => {
    dispatch(loginStart());
    api
      .post("auth/login", { email, password })
      .then(response => dispatch(loginSuccess(response.data.data.access_token)))
      .catch(error => dispatch(loginError(error)));
  };
};

export const loginStart = () => {
  return {
    type: ActionType.LOGIN_START
  };
};

export const loginSuccess = token => {
  return {
    type: ActionType.LOGIN_SUCCESS,
    token
  };
};

export const loginError = error => {
  return {
    type: ActionType.LOGIN_ERROR,
    error
  };
};
