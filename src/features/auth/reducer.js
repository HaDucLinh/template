import * as ActionType from "./actionTypes";
import { configHeader } from "../../common";

export const authReducer = (
  state = { isLogedIn: false, isLoading: false },
  action
) => {
  switch (action.type) {
    case ActionType.LOGIN_START:
      console.log(state);
      return {
        ...state,
        isLoading: true
      };
    case ActionType.LOGIN_SUCCESS:
      configHeader(action.token);
      return {
        ...state,
        isLogedIn: true
      };
    case ActionType.LOGIN_ERROR:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
};
