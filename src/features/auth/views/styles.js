import { StyleSheet } from "react-native";
import { Color } from "../../../constants";
import { Dimensions } from "react-native";

const windowSize = Dimensions.get("window");
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  button: {
    width: windowSize.width - 40,
    height: 50,
    margin: 20
  }
});
