import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import * as Actions from "../actions";
import { Button } from "../../../components";
import { styles } from "./styles";

class LoginView extends Component {
  static navigationOptions = {
    title: "Login"
  };

  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Login"
          style={styles.button}
          onPress={() => this.props.login("admin@example.com", "12345678")}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    // countValue: state.counter.currentCount,
    // hightlight: state.counter.hightlight
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: (email, password) => dispatch(Actions.login(email, password))
  };
};

export const LoginScreen = connect(mapStateToProps, mapDispatchToProps)(
  LoginView
);
