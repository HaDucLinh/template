import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { styles } from "./styles";
import * as Actions from "../actions";

class HomeView extends Component {
  static navigationOptions = {
    title: "Home"
  };

  render() {
    return (
      <View style={[...styles.container]}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    // countValue: state.counter.currentCount,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // increment: value => dispatch(Actions.increment(value))
  };
};

export const HomeScreen = connect(mapStateToProps, mapDispatchToProps)(
  HomeView
);
