import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  text: {
    color: "red",
    margin: 20,
    padding: 10,
    fontSize: 40
  },
  button: {
    height: 100,
    width: 200,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "green"
  }
});
