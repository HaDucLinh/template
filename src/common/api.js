import axios from "axios";

const URL_END_POINT = "http://api.baongocstore.com/v1";

export const configHeader = token => {
  api.defaults.headers.common["Authorization"] = token;
};

export const api = axios.create({
  baseURL: URL_END_POINT
});
