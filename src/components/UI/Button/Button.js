import React, { Component } from "react";
import { TouchableOpacity, Text } from "react-native";
import { styles } from "./styles";

export class Button extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[styles.button, this.props.style]}
        onPress={this.props.onPress}
      >
        <Text style={styles.text}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}
