import { StyleSheet } from "react-native";
import { Color } from "../../../constants";

export const styles = StyleSheet.create({
  button: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Color.blazeOrange,
    borderRadius: 5
  },
  text: {
    color: Color.white,
    fontSize: 20
  }
});
