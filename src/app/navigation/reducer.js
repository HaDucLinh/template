import { AppNavigator } from "./navigator";
import { LOGIN_SUCCESS } from "../../features";

const initialState = AppNavigator.router.getStateForAction(
  AppNavigator.router.getActionForPathAndParams("login")
);

export const navReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return AppNavigator.router.getStateForAction(
        AppNavigator.router.getActionForPathAndParams("home"),
        state
      );
    default:
  }

  const nextState = AppNavigator.router.getStateForAction(action, state);
  return nextState || state;
};
