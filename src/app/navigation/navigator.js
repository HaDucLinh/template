import { StackNavigator } from "react-navigation";
import { LoginScreen, HomeScreen } from "../../features";
import { Color } from "../../constants";

export const AppNavigator = StackNavigator(
  {
    login: {
      screen: LoginScreen
    },
    home: {
      screen: HomeScreen
    }
  },
  {
    navigationOptions: {
      headerStyle: {
        backgroundColor: Color.blazeOrange
      },
      headerTintColor: Color.white,
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);
