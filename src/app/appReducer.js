import { navReducer } from "./navigation";
import { authReducer } from "../features/auth";
import { combineReducers } from "redux";

export const appReducer = combineReducers({
  nav: navReducer,
  auth: authReducer
});
