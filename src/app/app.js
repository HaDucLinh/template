import { createReduxBoundAddListener } from "react-navigation-redux-helpers";
import { Provider, connect } from "react-redux";
import React from "react";
import { AppNavigator } from "./navigation";
import { store } from "./store";
import { addNavigationHelpers } from "react-navigation";

const addListener = createReduxBoundAddListener("root");

class App extends React.Component {
  render() {
    return (
      <AppNavigator
        navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.nav,
          addListener
        })}
      />
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav
});

const AppWithNavigationState = connect(mapStateToProps)(App);

export class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}
