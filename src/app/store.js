import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { appReducer } from "./appReducer";
import { navMiddleware } from "./navMiddleware";

export const store = createStore(
  appReducer,
  composeWithDevTools(applyMiddleware(navMiddleware, thunk))
);
